import { Component, OnInit } from '@angular/core';
import * as AWSCognito from 'amazon-cognito-identity-js';

@Component({
  selector: 'app-icentra-auth',
  templateUrl: './icentra-auth.component.html',
  styleUrls: ['./icentra-auth.component.css']
})
export class IcentraAuthComponent implements OnInit {

	processLogs = [];
	usernameInput: any;
	passwordInput: any;
	userPool : any;
	authDetails : any;
	cognitoUser : any;
  usrLoginDetails : any;
  poolData = {UserPoolId : 'ap-south-1_EL2yohyKJ', ClientId : '300gt5i9s7sjjovkrrktsjf3q1'};
  constructor() { }

  ngOnInit() {
  }

  doLogin(){

  	//validation logics are skipped because this is fast quick POC for cognito check.
  	if (this.usernameInput && this.usernameInput.length > 3 && this.passwordInput && this.passwordInput.length > 3)
    {
    	this.usrLoginDetails = {Username:this.usernameInput, Password:this.passwordInput};
			this.userPool = new AWSCognito.CognitoUserPool(this.poolData);
			// Later on
			this.authDetails = new AWSCognito.AuthenticationDetails(this.usrLoginDetails);
			this.cognitoUser = new AWSCognito.CognitoUser({
				Username: this.usrLoginDetails.Username,
				Pool: this.userPool
			});

			this.cognitoUser.authenticateUser(this.authDetails, {
			  onSuccess: (result) => {
			  	this.processLogs = [...this.processLogs, `access token = ${result.getAccessToken().getJwtToken()}`];
			  },
			  onFailure: (err) => {
			  	this.processLogs = [...this.processLogs, err.message];
			  },
			  newPasswordRequired: (userAttributes, requiredAttributes) => {
			  	this.processLogs = [...this.processLogs, "Your Temporary Password Is Ok, We Are Completing The New Password Challenge Using That Same Temporary Password"];
			  	this.cognitoUser.completeNewPasswordChallenge(this.usrLoginDetails.Password, null, {
						onSuccess: (result) => {
							this.processLogs = [...this.processLogs, `access token = ${result.accessToken.jwtToken}`];
						},
						onFailure: (error) => {
			  			this.processLogs = [...this.processLogs, error.message];
						}
					});
			  },
				mfaRequired: () => {},  // no-op
	      customChallenge: () => {} // no-op
			});
    }
    else
    {
    	this.processLogs = [...this.processLogs, "Please provide vaild username, passwords"];
    }
  }

}
