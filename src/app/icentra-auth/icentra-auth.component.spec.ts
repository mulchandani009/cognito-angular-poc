import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcentraAuthComponent } from './icentra-auth.component';

describe('IcentraAuthComponent', () => {
  let component: IcentraAuthComponent;
  let fixture: ComponentFixture<IcentraAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcentraAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcentraAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
