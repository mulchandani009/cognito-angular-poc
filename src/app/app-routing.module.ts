import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IcentraAuthComponent } from './icentra-auth/icentra-auth.component';

const routes: Routes = [
						{ path: '', component: IcentraAuthComponent}
					];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
